package rav.bills.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import rav.bills.db.bill.model.Bill;
import rav.bills.db.bill.model.BillStatus;
import rav.bills.db.bill.model.Company;
import rav.bills.db.bill.model.SettlementType;
import rav.bills.db.bill.repository.BillRepository;
import rav.bills.db.bill.repository.CompanyRepository;

import java.time.YearMonth;
import java.util.Date;

@Component
public class BootStrapData implements CommandLineRunner {

    private final BillRepository billRepository;
    private final CompanyRepository companyRepository;

    public BootStrapData(BillRepository billRepository, CompanyRepository companyRepository) {
        this.billRepository = billRepository;
        this.companyRepository = companyRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Started in Bootstrap");
        Company pgnig = new Company("PGNiG Obrót detaliczny Sp. z o.o.", "PKO Bank Polski SA", "3110201026");
        Company innogy = new Company("innogy Stoen Operator Sp. z o.o..", "Alior Bank SA", "6912406960");
        companyRepository.save(pgnig);
        companyRepository.save(innogy);

        Bill gas = new Bill("PGNiG Obrót detaliczny", 79.38, pgnig, new Date(),  new Date(), YearMonth.of(2021,1),new Date(), SettlementType.MONTHLY, BillStatus.UPLOADED);
        Bill energy = new Bill("Innogy prąd", 200.0, innogy, new Date(), new Date(), YearMonth.of(2021,1), new Date(),  SettlementType.QUARTERLY, BillStatus.UPLOADED);
        billRepository.save(energy);
        billRepository.save(gas);

        System.out.println("Finished in Bootstrap");
    }
}
