package rav.bills.db.bill.converter;

import rav.bills.db.bill.model.SettlementType;

import javax.persistence.AttributeConverter;


public class SettlementTypeConverter implements AttributeConverter<SettlementType, Integer> {
    @Override
    public Integer convertToDatabaseColumn(SettlementType settlementType) {
        if (settlementType == null)
            return null;

        switch (settlementType) {
            case MONTHLY:
                return 1;

            case TWO_MONTH:
                return 2;

            case QUARTERLY:
                return 3;

            case SEMI_ANNUAL:
                return 6;

            case ANNUAL:
                return 12;

            default:
                throw new IllegalArgumentException(settlementType + " not supported.");
        }
    }

    @Override
    public SettlementType convertToEntityAttribute(Integer integer) {
        if (integer == null)
            return null;

        switch (integer) {
            case 1:
                return SettlementType.MONTHLY;

            case 2:
                return SettlementType.TWO_MONTH;

            case 3:
                return SettlementType.QUARTERLY;

            case 6:
                return SettlementType.SEMI_ANNUAL;

            case 12:
                return SettlementType.ANNUAL;

            default:
                throw new IllegalArgumentException(integer + " not supported.");
        }
    }
}
