package rav.bills.db.bill.converter;

import javax.persistence.AttributeConverter;
import java.time.YearMonth;
import java.time.format.DateTimeParseException;

public class YearMonthConverter  implements AttributeConverter<YearMonth, String> {
    @Override
    public String convertToDatabaseColumn(YearMonth yearMonth) {
        if (yearMonth == null)
            return null;

        return yearMonth.toString();
    }

    @Override
    public YearMonth convertToEntityAttribute(String s) {
        if (s == null)
            return null;

        YearMonth yearMonth = null;
        try {
            yearMonth = YearMonth.parse(s);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
        }
        return yearMonth;
    }
}
