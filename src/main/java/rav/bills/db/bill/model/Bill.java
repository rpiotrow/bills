package rav.bills.db.bill.model;

import org.hibernate.annotations.CreationTimestamp;
import rav.bills.db.bill.converter.SettlementTypeConverter;
import rav.bills.db.bill.converter.YearMonthConverter;

import javax.persistence.*;
import java.time.YearMonth;
import java.util.Date;
import java.util.Objects;

@Entity
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;
    private Double value;
    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @CreationTimestamp
    private Date uploadDate;

    @Temporal(TemporalType.DATE)
    private Date paidDate;

    @Convert(converter = YearMonthConverter.class)
    private YearMonth reportDate;

    @Temporal(TemporalType.DATE)
    private Date deadline;

    @Convert(converter = SettlementTypeConverter.class)
    private SettlementType settlementType;
    private BillStatus status;

    public Bill() {
    }

    public Bill(String title, Double value, Company company, Date uploadDate, Date paidDate, YearMonth reportDate,
                Date deadline, SettlementType settlementType, BillStatus status) {
        this.title = title;
        this.value = value;
        this.company = company;
        this.uploadDate = uploadDate;
        this.paidDate = paidDate;
        this.reportDate = reportDate;
        this.deadline = deadline;
        this.settlementType = settlementType;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public double getValue() {
        return value;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Date getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Date sentDate) {
        this.paidDate = paidDate;
    }

    public YearMonth getReportDate() {
        return reportDate;
    }

    public void setReportDate(YearMonth reportDate) {
        this.reportDate = reportDate;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public SettlementType getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(SettlementType settlementType) {
        this.settlementType = settlementType;
    }

    public BillStatus getStatus() {
        return status;
    }

    public void setStatus(BillStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", value=" + value +
                ", uploadDate=" + uploadDate +
                ", paidDate=" + paidDate +
                ", reportDate=" + reportDate +
                ", deadline=" + deadline +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bill bill = (Bill) o;

        return Objects.equals(id, bill.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
