package rav.bills.db.bill.model;

public enum BillStatus {
    UPLOADED, PAID, CANCELED
}
