package rav.bills.db.bill.model;

public enum SettlementType {
    MONTHLY, TWO_MONTH, QUARTERLY, SEMI_ANNUAL, ANNUAL
}
