package rav.bills.db.bill.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rav.bills.db.bill.model.Bill;

public interface BillRepository extends JpaRepository<Bill, Long> {
}
