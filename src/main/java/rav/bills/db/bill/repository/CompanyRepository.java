package rav.bills.db.bill.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rav.bills.db.bill.model.Company;

public interface CompanyRepository extends JpaRepository<Company, Long> {
}
