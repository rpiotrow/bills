package rav.bills.mainViewController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import rav.bills.db.bill.model.Bill;
import rav.bills.db.bill.repository.BillRepository;

@Controller
public class MainViewController {

    private final BillRepository billRepository;

    public MainViewController(BillRepository billRepository) {
        this.billRepository = billRepository;
    }

    @RequestMapping("/bills")
    public String getBills(Model model) {

        model.addAttribute("bills", billRepository.findAll());

        return "bills/list";
    }

    @RequestMapping("/bills/{billId}")
    public String showBill(@PathVariable Long billId, Model model) {

        Bill bill = billRepository.getOne(billId);

        model.addAttribute("bill", bill);

        return "bills/details";
    }
}
